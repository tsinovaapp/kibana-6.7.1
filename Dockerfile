# Imagem do ubuntu
FROM ubuntu:16.04

# responsável
MAINTAINER Leonardo Barbosa <leobar1995@gmail.com>

# atualiza o O.S
RUN apt-get update

# Instala as dependências
RUN apt-get install git -y
RUN apt-get -y install software-properties-common
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 \
    select true | debconf-set-selections && \
    add-apt-repository -y ppa:webupd8team/java && \
    apt-get update && \    
    apt-get install -y oracle-java8-installer && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/cache/oracle-jdk8-installer

# Seta a variável de ambiente Java
ENV JAVA_HOME=/usr/lib/jvm/java-8-oracle


ARG CACHEBUST=1


# Instala o kibana
RUN cd /usr/share/
RUN cd /usr/share/ && \
    git clone https://Leo1995@bitbucket.org/tsinovaapp/kibana-6.7.1.git kibana && \
    chmod 777 -Rf kibana/

# Remover pastas e arquivos desnecessários
RUN rm /usr/share/kibana/config/kibana.yml

# Adiciona arquivo de configuração
ADD ./config/kibana.yml /usr/share/kibana/config/kibana.yml

# Permissão geral na pasta
RUN chmod 777 -Rf /usr/share/kibana

# Diretório inicial de trabalho
WORKDIR /usr/share/kibana

# expoẽ a porta para uso
EXPOSE 5601

# executa o kibana
ENTRYPOINT /usr/share/kibana/bin/kibana
