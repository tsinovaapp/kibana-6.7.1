export default function (server) {

  server.route({
    path: '/api/cftv_plugin/example',
    method: 'GET',
    handler(req, reply) {
      reply({ time: (new Date()).toISOString() });
    }
  });

}
