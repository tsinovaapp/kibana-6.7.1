import $ from 'jquery';
var md5 = require('md5');

$(document).ready(function() {

  if (window.location.pathname != "/app/loginapp") {
    var user_name = (localStorage.getItem("user_logged") === null ? '' : JSON.parse(localStorage.getItem("user_logged")).name).split(" ")[0];
    var user_logged_div = '<div class="kbnGlobalNavLink" ng-class="{ active: isActive, \'kbnGlobalNavLink-isDisabled\': isDisabled }" tooltip="" tooltip-placement="right" tooltip-popup-delay="0" tooltip-append-to-body="1" ng-repeat="link in switcher.links" ng-if="!link.hidden" is-active="link.active" is-disabled="link.disabled" tooltip-content="switcher.getTooltip(link)" on-click="switcher.ensureNavigation($event, link)" url="link.active ? link.url : (link.lastSubUrl || link.url)" icon="link.icon"' + 'label="link.title">' +
      '<a class="kbnGlobalNavLink__anchor" href="#" ng-click="onClick({ $event: $event })" data-test-subj="appLink">' +
      '<div class="kbnGlobalNavLink__icon">' +
      '<img ng-if="icon" height="26" style="margin-top:8px" class="kbnGlobalNavLink__euiIcon ng-scope" kbn-src="/plugins/loginapp/user_logged.png" aria-hidden="true" src="../plugins/loginapp/user_logged.png">' +
      '</div>' +
      '<div class="kbnGlobalNavLink__title ng-binding">' +
      'Olá, ' + user_name +
      '</div>' +
      '</a>' +
      '</div>';
    $('.kbnGlobalNav__linksSection:first > app-switcher').prepend(user_logged_div);
  }

  $("#login").submit(function(event) {

    event.preventDefault();

    var email = $("#email").val();
    var password = $("#password").val();

    $.ajax({
      url: "../api/loginapp/authentication",
      method: "POST",
      headers: {
        'kbn-xsrf': 'reporting',
        'Content-Type': 'application/json'
      },

      data: JSON.stringify({
        email: email,
        password: md5(password)
      }),

      success: function(data) {
        if ($.isEmptyObject(data)) {
          mensagemErro("Credenciais inválidas. Tente Novamente.");
          $('#loading').replaceWith('<button className="btn btn-primary btn-block" id="entrar" type="submit">Entrar</button>');
        } else {
          var user_logged = {
            "name": data[0]._source.name,
            "email": email,
            "functionalities": ""
          };
          localStorage.setItem("user_logged", JSON.stringify(user_logged));
          if (localStorage.getItem("url")) {
            window.location = localStorage.getItem("url");
          } else {
            window.location = "kibana";
          }
        }
      },

      error: function(err) {
        $('#loading').replaceWith('<button className="btn btn-primary btn-block" id="entrar" type="submit">Entrar</button>');
        mensagemErro("Credenciais inválidas. Tente Novamente.");
      },

      beforeSend: function() {
        $('#entrar').replaceWith('<img id="loading" kbn-src="/plugins/loginapp/preloading.apng" aria-hidden="true" src="../plugins/loginapp/preloading.apng" style="align: center; width: 35px;">');
      }

    });

    return false;

  });

  $("a[aria-label=Sair]").click(function(e) {
    e.preventDefault();
    localStorage.removeItem("user_logged");
    window.location = "loginapp";
  });

});

if (window.location.pathname.indexOf("loginapp") > -1) {
  if (localStorage.getItem("user_logged") !== null) {
    window.location = "kibana";
  }
} else {
  if (localStorage.getItem("user_logged") === null) {
    localStorage.setItem("url", window.location);
    window.location = "loginapp";
  }
}

function mensagemErro(mensagem) {
  var div = document.getElementById('erro');
  div.style.display = 'block';
  div.innerHTML = mensagem;
  setTimeout(function() {
    div.style.display = 'none';
    div.innerHTML = '';
  }, 10000);
}
