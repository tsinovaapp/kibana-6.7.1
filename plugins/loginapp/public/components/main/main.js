import React from 'react';
import {
  EuiPage,
  EuiPageHeader,
  EuiTitle,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentHeader,
  EuiPageContentBody,
  EuiText
} from '@elastic/eui';

export class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    /*
       FOR EXAMPLE PURPOSES ONLY.  There are much better ways to
       manage state and update your UI than this.
    */
    const { httpClient } = this.props;
    httpClient.get('../api/loginapp/example').then((resp) => {
      this.setState({ time: resp.data.time });
    });
  }
  render() {
    const { title } = this.props;
    return (
  <EuiPage>
    <EuiPageBody>
            <div className="login-clean">

             <form id="login">

               <div className="illustration">
                    <div id="logo" className="fluid"> </div>
               </div>

           <div className="form-group">
           <input type="email" name="email" placeholder="E-mail" className="form-control" id="email"/>
           </div>

          <div className="form-group">
          <input type="password" name="password" placeholder="Senha" className="form-control" id="password"/>
          </div>

          <div className="form-group">
          <div id="erro" style={{display: 'none', color: '#871111', font:'10px'}}></div>
          </div>

           <div className="form-group" id="form-submit" style={{textAlign: 'center'}}>
           <button  className="btn btn-primary btn-block" id="entrar" type="submit">Entrar</button>
           </div>

            <a href="#" className="forgot">Esqueceu seu e-mail ou senha?</a>
            <br/>
            <br/>
            <span className="forgot" style={{alignItems: 'center', color: '#c4c4c4', fontSize: '10px'}}>Powered by © TSInova</span>
          </form>

        </div>
    </EuiPageBody>
  </EuiPage>
);
  }
}
