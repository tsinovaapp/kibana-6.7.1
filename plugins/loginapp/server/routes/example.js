export default function (server) {

  server.route({
    path: '/api/loginapp/example',
    method: 'GET',
    handler() {
      return { time: (new Date()).toISOString() };
    }
  });

  // retorna usuário que possui as seguintes credenciais
  server.method('getUser', (req, h, email, password) => {
      return new Promise((resolve, reject) => {
        const dataCluster = server.plugins.elasticsearch.getCluster('data');
        dataCluster.callWithRequest(req, 'search', {
          index: 'users',
          q:"email:\""+email+"\" AND password:\""+password+"\""
        }).then(res => {
          resolve(res.hits.hits);
        });
      });
  });

  // rota para autenticar o usuário
  server.route({
    path: '/api/loginapp/authentication',
    method: 'POST',
    handler: async (req, h) => {
      try {
        const result = await server.methods.getUser(req, h, req.payload.email, req.payload.password);
        return result;
      } catch (err) {
        throw err;
      }
    }
  });

}
