import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'redes_plugin',
    uiExports: {
      app: {
        title: 'Redes',
        description: 'Visualização de métricas de switches e roteadores',
        main: 'plugins/redes_plugin/app',
        styleSheetPath: require('path').resolve(__dirname, 'public/app.scss'),
        url: "/app/kibana#/dashboard/redes",
        icon: 'plugins/redes_plugin/icon.svg',
      },
      hacks: [
        'plugins/redes_plugin/hack'
      ]
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
