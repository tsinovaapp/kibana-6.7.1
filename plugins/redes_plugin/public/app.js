import React from 'react';
import { uiModules } from 'ui/modules';
import chrome from 'ui/chrome';
import { render, unmountComponentAtNode } from 'react-dom';

import 'ui/autoload/styles';
import './less/main.less';
import { Main } from './components/main';

import $ from 'jquery';

$(() => {
	const $appSwitcher = $('app-switcher');
	function moveTab(tabLabel) {
		$appSwitcher.find(`.global-nav-link:contains('${tabLabel}')`).appendTo($appSwitcher);
	}

	//['Sistemas','Dashboard','Visualize','Discover','Management', 'Timelion','Dev Tools'].forEach((label) => moveTab(label));

	function hide(tabLabel){
		$appSwitcher.find(`.global-nav-link:contains('${tabLabel}')`).remove();
	}

	['Discover','Management', 'Timelion','Dev Tools'].forEach((label) => hide(label));

});

const app = uiModules.get('apps/redesPlugin');

app.config($locationProvider => {
  $locationProvider.html5Mode({
    enabled: false,
    requireBase: false,
    rewriteLinks: false,
  });
});
app.config(stateManagementConfigProvider =>
  stateManagementConfigProvider.disable()
);

function RootController($scope, $element, $http) {
  const domNode = $element[0];

  // render react to DOM
  render(<Main title="redes-plugin" httpClient={$http} />, domNode);

  // unmount react on controller destroy
  $scope.$on('$destroy', () => {
    unmountComponentAtNode(domNode);
  });
}

chrome.setRootController('redesPlugin', RootController);
