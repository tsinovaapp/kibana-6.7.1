export default function (server) {

  server.route({
    path: '/api/devices/example',
    method: 'GET',
    handler(req, reply) {
      return { time: (new Date()).toISOString() };
    }
  });

  // ---------------------------------------------------------------------------------------------------------------

  // deleta a categoria
  server.method('deleteCategory', (req, h) => {
      return new Promise((resolve, reject) => {

        const dataCluster = server.plugins.elasticsearch.getCluster('data');
        var macaddress = req.payload.macaddress;
        console.log("DELETE_CATEGORY = " + macaddress);

        dataCluster.callWithRequest(req, 'search', {
          index: "snmpts*",
          body: {
              "query": {
                  "match": {
                      "device.mac.keyword": {
                          "query": macaddress
                      }
                  }
              },
              "size": 1,
              "sort": [
                  {
                      "@timestamp": {
                          "order": "desc"
                      }
                  }
              ]
          }
        }).then(res => {
          var dataDevice = res.hits.hits[0];
          dataCluster.callWithRequest(req, 'transport.request', {
              path: '/' + dataDevice._index + '/doc/' + dataDevice._id + "/_update",
              method: 'POST',
              body: {
                "script": "ctx._source.device.remove(\"category\")"
              }
            }).then(res2 => {
              console.log("RESPOSTA DELETE category: " + res2);
              if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
                 resolve({"success": true, "message": "Categoria removida com sucesso!"});
               }else{
                 resolve({"success": false, "message": "Falha ao tentar remover a categoria!"});
               }
            });
        });

      });
  });

  // ---------------------------------------------------------------------------------------------------------------

  // rota para deletar a categoria
  server.route( {
    path: '/api/devices/delete/category',
    method: 'PUT',
    handler: async (req, h) => {
        try {
          const result = await server.methods.deleteCategory(req, h);
          return result;
        } catch (err) {
          throw err;
        }
      }
  });

  // ---------------------------------------------------------------------------------------------------------------

  // deleta o sistema
  server.method('deleteSystems', (req, h) => {
      return new Promise((resolve, reject) => {
        const dataCluster = server.plugins.elasticsearch.getCluster('data');
        var macaddress = req.payload.macaddress;
        console.log("DELETE_SYSTEMS = " + macaddress);

        dataCluster.callWithRequest(req, 'search', {
          index: "snmpts*",
          body: {
              "query": {
                  "match": {
                      "device.mac.keyword": {
                          "query": macaddress
                      }
                  }
              },
              "size": 1,
              "sort": [
                  {
                      "@timestamp": {
                          "order": "desc"
                      }
                  }
              ]
          }
        }).then(res => {
          var dataDevice = res.hits.hits[0];
          dataCluster.callWithRequest(req, 'transport.request', {
              path: '/' + dataDevice._index + '/doc/' + dataDevice._id + "/_update",
              method: 'POST',
              body: {
                "script": "ctx._source.device.remove(\"systems\")"
              }
            }).then(res2 => {
              console.log("RESPOSTA DELETE systems: " + res2);
              if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
                 resolve({"success": true, "message": "Sistemas removido com sucesso!"});
               }else{
                 resolve({"success": false, "message": "Falha ao tentar remover os sistemas!"});
               }
            });
        });

      });
  });

  // ---------------------------------------------------------------------------------------------------------------

  // rota para deletar sistema
  server.route( {
    path: '/api/devices/delete/systems',
    method: 'PUT',
    handler: async (req, h) => {
        try {
          const result = await server.methods.deleteSystems(req, h);
          return result;
        } catch (err) {
          throw err;
        }
      }
  });

  // ---------------------------------------------------------------------------------------------------------------

  // atualiza o dispositivo
  server.method('updateDevice', (req, h, macaddress, category, systems) => {
      return new Promise((resolve, reject) => {
        const dataCluster = server.plugins.elasticsearch.getCluster('data');
        console.log("UPDATE_DEVICE = " + macaddress + "; " + category + "; " + systems);
        dataCluster.callWithRequest(req, 'search', {
          index: "snmpts*",
          body: {
              "query": {
                  "match": {
                      "device.mac.keyword": {
                          "query": macaddress
                      }
                  }
              },
              "size": 1,
              "sort": [
                  {
                      "@timestamp": {
                          "order": "desc"
                      }
                  }
              ]
          }
        }).then(res => {

          var dataDevice = res.hits.hits[0];
          var dataDeviceUpdate;

          if (category == undefined && systems == undefined){
            dataDeviceUpdate = null;

          }else if (category != undefined && systems == undefined){
            dataDeviceUpdate = {
                "doc": {
                    "device": {
                        "category": category
                    }
                }
            };

          }else if (category == undefined && systems != undefined){
            dataDeviceUpdate = {
                "doc": {
                    "device": {
                        "systems": systems
                    }
                }
            };

          }else{
            dataDeviceUpdate = {
                "doc": {
                    "device": {
                        "category": category,
                        "systems": systems
                    }
                }
            };

          }

          if (dataDeviceUpdate == null){
            resolve({"success": false, "message": "Informar a categoria e/ou o sistema do dispositivo"});

          }else{
            dataCluster.callWithRequest(req, 'transport.request', {
              path: '/' + dataDevice._index + '/doc/' + dataDevice._id + '/_update',
              method: 'POST',
              body: dataDeviceUpdate
            }).then(res2 => {
              console.log("RESPOSTA UPDATE device: " + res2);
             if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
                resolve({"success": true, "message": "Informações do dispositivo atualizada com sucesso!"});
              }else{
                resolve({"success": false, "message": "Falha ao atualizar os dados do dispositivo!"});
              }
            });

          }

        });

      });
  });

  // ---------------------------------------------------------------------------------------------------------------

  // rota para atualizar o device
  server.route({
    path: '/api/devices/update',
    method: 'PUT',
    handler: async (req, h) => {

      var macaddress = req.payload.macaddress;
      var category = req.payload.category;
      var systems = req.payload.systems;

      if (category == undefined && systems == undefined){
        return {"success": false, "message": "Informar a categoria e/ou o sistema do dispositivo"};
      }

      try {
        const result = await server.methods.updateDevice(req, h, macaddress, category, systems);
        return result;
      } catch (err) {
        throw err;
      }

    }

  });

  // ---------------------------------------------------------------------------------------------------------------


  // retorna a lista de dispositivos
  server.method('getListDevices', (req, h) => {
      return new Promise((resolve, reject) => {
        const dataCluster = server.plugins.elasticsearch.getCluster('data');
        dataCluster.callWithRequest(req, 'search', {
          index: "snmpts*",
          body: {
            "size": 0,
             "query": {
                 "bool": {
                     "should": [
                         {
                             "exists": {
                                 "field": "device.mac"
                             }
                         }
                     ]
                 }
             },
             "aggs": {
                 "group": {
                     "terms": {
                         "field": "device.mac.keyword",
                         "size": 2147483647
                     },
                     "aggs": {
                         "group_docs": {
                             "top_hits": {
                                 "sort": [
                                     {
                                         "@timestamp": {
                                             "order": "desc"
                                         }
                                     }
                                 ],
                                 "size": 1
                             }
                         }
                     }
                 }
             }
         }
        }).then(res => {
          resolve(res.aggregations.group.buckets);
        });

      });
  });

  // ---------------------------------------------------------------------------------------------------------------

  // rota para listar os dispositivos
  server.route({
   path: '/api/devices/list',
   method: 'GET',
   handler: async (req, h) => {
     try {
       const result = await server.methods.getListDevices(req, h);
       return result;
     } catch (err) {
       throw err;
     }
   }
 });


}
