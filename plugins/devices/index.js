import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'devices',
    uiExports: {
      app: {
        title: 'Devices',
        description: 'Dispositivos',
        main: 'plugins/devices/app',
        styleSheetPath: require('path').resolve(__dirname, 'public/app.scss'),
        icon: 'plugins/devices/icon.svg',
        listed: false
      },
      hacks: [
        'plugins/devices/hack'
      ]
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
