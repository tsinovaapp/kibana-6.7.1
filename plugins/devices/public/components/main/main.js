import React from 'react';
import Select from 'react-select';
import { UncontrolledAlert } from 'reactstrap';

import ReactTable from "react-table";
import 'react-table/react-table.css';

import {
  EuiPage,
  EuiPageHeader,
  EuiTitle,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentHeader,
  EuiPageContentBody,
  EuiText
} from '@elastic/eui';

export class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      devicesList: null,
      //devicesFilteredList: null,
      // filter: 'ip',
      // filterValue: '',
      selectedOption: null,
      ipSwitchOptions: []
    };

    this.handleChangeCategory = this.handleChangeCategory.bind(this);
    this.handleChangeSystems = this.handleChangeSystems.bind(this);
    this.updateDevice = this.updateDevice.bind(this);
    // this.renderFilter = this.renderFilter.bind(this);
    // this.renderFilterInput = this.renderFilterInput.bind(this);
    // this.onFilterChange = this.onFilterChange.bind(this);
    // this.onFilterValueChange = this.onFilterValueChange.bind(this);
    // this.handleSearch = this.handleSearch.bind(this);
  }

  componentWillMount() {
    this.fetchDevices();
  }

  fetchDevices() {
    const { httpClient } = this.props;
    httpClient.get('../api/devices/list')
    .then(response => {
      console.log('fetched');
      const devices = response.data.map((device, key) => {
        const { key: id } = device;
        const { host: ipSwitch, ports: { port }, device: { vendor, ip, mac, os_details: os, category, systems, host_name } } = device.group_docs.hits.hits[0]._source;
        this.setState({ [id]: { category: category ? this.getCategory(category) : [], systems: systems && systems.length > 0 ? this.getSystems(systems) : [], mac } })
        return {
          name: host_name,
          id,
          ipSwitch,
          port,
          vendor,
          ip,
          mac,
          os,
          category: category ? this.getCategory(category) : [],
          systems: systems && systems.length > 0 ? this.getSystems(systems) : [],
          categorySelect: (value, index) => this.renderSelectCategory(id, value, index),
          systemsSelect: (value, index) => this.renderSelectSystems(id, value, index)
        };
      });
      this.setState({ devicesList: devices });

      //Make the array of ip switches
      const ipSwitchOptions = devices.map(device => {
        return device.ipSwitch;
      })
      .filter((value, index, self) => {
        return self.indexOf(value) === index;
      });
      this.setState({ ipSwitchOptions });
    })
    .catch(err => {
      console.log(err);
    })
  }

  getSystems(systems) {
    return systems.map(system => {
      return options.find(option => {
        return option.value === system;
      })
    })
  }

  getCategory(category) {
    return typeoptions.find(option => {
      return option.value === category;
    });
  }

  updateDevice(state) {
    const { httpClient } = this.props;
    const { category: { value: categoryValue }, systems, mac: macaddress } = this.state[state];
    const systemsArray = systems.map(system => {
      return system.value;
    });
    httpClient.put('../api/devices/update', { category: categoryValue, macaddress, systems: systemsArray })
    .then(response => {
      console.log(response);
    })
    .catch(err => {
      console.log(err);
    })
  }

  deleteDeviceCategory(state){
    const { httpClient } = this.props;
    const { mac: macaddress} = this.state[state];
    httpClient.put('../api/devices/delete/category', { macaddress })
    .then(response => {
      console.log(response);
    })
    .catch(err => {
      console.log(err);
    })
  }

  deleteDeviceSystems(state){
    const { httpClient } = this.props;
    const { mac: macaddress} = this.state[state];
    httpClient.put('../api/devices/delete/systems', { macaddress })
    .then(response => {
      console.log(response);
    })
    .catch(err => {
      console.log(err);
    })
  }

  renderSelectCategory(state, value, index) {
    return (
       <Select
          value={value}
          key={`category${state}`}
          onChange={selectedOption => this.handleChangeCategory(selectedOption, state, index)}
          options={typeoptions}
          isMulti={false}
          isSearchable={false}
          // defaultValue={this.state[state].category}
          isClearable={true}
          placeholder='Selecione'
      />
    )
  }

  renderSelectSystems(state, value, index) {
    return (
       <Select
          value={value}
          key={`systems${state}`}
          onChange={selectedOptions => this.handleChangeSystems(selectedOptions, state, index)}
          options={options}
          isMulti={true}
          isSearchable={false}
          // defaultValue={this.state[state].systems}
          placeholder='Selecione'
      />
    )
  }

  async handleChangeCategory(option, state, index) {
    await this.setState(prevState => ({ [state]: { ...prevState[state], category: option || [] } }));
    // const newDevices = [ ...this.state.devicesFilteredList ];
    // newDevices[index] = { ...newDevices[index], category: option };
    // await this.setState({ devicesFilteredList: newDevices });

    const newDevicesList = [ ...this.state.devicesList ];
    newDevicesList[index] = { ...newDevicesList[index], category: option};
    await this.setState({ devicesList: newDevicesList })

    if(option === null || option.length === 0){
      this.deleteDeviceCategory(state);
    } else {
      this.updateDevice(state);
    }
  }

  async handleChangeSystems(options, state, index) {
    await this.setState(prevState => ({ [state]: { ...prevState[state], systems: options } }));
    // const newDevices = [...this.state.devicesFilteredList];
    // newDevices[index] = { ...newDevices[index], systems: options };
    // await this.setState({ devicesFilteredList: newDevices });

    const newDevicesList = [ ...this.state.devicesList ];
    newDevicesList[index] = { ...newDevicesList[index], systems: options};
    await this.setState({ devicesList: newDevicesList })

    if(options === null || options.length === 0){
      this.deleteDeviceSystems(state);
    } else {
      this.updateDevice(state);
    }
  }

  // async onFilterChange(option) {
  //   await this.setState({ filter: option ? option.value : null });
  // }

  // async onFilterValueChange(value) {
  //   if (value.target) {
  //     await this.setState({ filterValue: value.target.value });
  //   } else if (value.value) {
  //     await this.setState({ filterValue: value.value });
  //   } else {
  //     await this.setState({ filterValue: '' });
  //   }
  //   this.handleSearch();
  // }

  // async handleSearch() {
  //   const { filter, filterValue, devicesList } = this.state;

  //   if (!filterValue) {
  //     console.log('value vazio');
  //     await this.setState({ devicesFilteredList: devicesList });
  //   } else {
  //     let devicesFilteredList = [];
  //     switch(filter) {
  //       case 'ip':
  //       case 'mac': {
  //         devicesFilteredList = devicesList.filter(device => {
  //           return device[filter].indexOf(filterValue) === 0;
  //         });
  //         break;
  //       }
  //       case 'port': {
  //         devicesFilteredList = devicesList.filter(device => {
  //           return device[filter] == filterValue;
  //         });
  //         break;
  //       }
  //     }

  //     await this.setState({ devicesFilteredList });
  //   }
  // }

  getTdProps(state, rowInfo, column) {
    const { id } = column;
    const columns = ['categorySelect', 'systemsSelect'];
    return {
      style: columns.indexOf(id) > -1 ? { overflow: 'visible' } : {}
    }
  }

  getTheadFilterTrProps(state) {
    return {
      style: { overflow: 'visible' }
    }
  }

  renderTable() {
    if (this.state.devicesList) {
      return (

        <ReactTable
          data={this.state.devicesList}
          columns={columns(this.state.ipSwitchOptions)}
          defaultPageSize={10}
          minRows={10}
          getTdProps={this.getTdProps}
          getTheadFilterTrProps={this.getTheadFilterTrProps}
          className='-striped -highlight'
          previousText='Anterior'
          nextText='Próximo'
          loadingText='Carregando...'
          noDataText='Nenhum resultado encontrado'
          pageText='Página'
          ofPage='de'
          rowsText='linhas'

        />
      );
    }
    return <div style={{ width: '100%', textAlign: 'center', marginTop: '30px', minHeight: '200px;' }}>Carregando...</div>
  }

/*  renderFilter() {
    if (this.state.devicesList) {
      return (
        <div className="col-md-5" style={{ marginTop: '30px', display: 'flex', background: '#fff9ef',  borderRadius: '5px', minHeight: '55px', marginBottom: '30px', flexDirection: 'row', float: 'right', alignItems: 'center' }}>
          <div className="col-md-3">
           Filtrar por:
          </div>

          <div className="col-md-4">
            <Select
              onChange={this.onFilterChange}
              options={filtro}
              isMulti={false}
              isSearchable={false}
              defaultValue={{label: 'IP', value: 'ip'}}
            />
          </div>

          { this.renderFilterInput() }

        </div>
      );
    }
  }*/


/*  renderFilterInput() {
    let optionsfilter = [];

    switch(this.state.filter) {
      case 'mac':
      case 'ip':
      case 'port':
      default:
        return (
          <div className="col-md-5">
            <input type='text' onChange={this.onFilterValueChange} className='form-control' value={this.state.filterValue} style={{ minHeight: '38px'}}/>
          </div>
        );

      case 'ipSwitch':
      case 'category':
      case 'systems':

      if (this.state.filter == 'ipSwitch') {
        optionsfilter = ipSwitchOptions;
      } else if (this.state.filter == 'category') {
        optionsfilter = typeoptions;
      } else {
        optionsfilter = options;
      }
        return (
          <div className="col-md-5">
               <Select
                  onChange={this.onFilterValueChange}
                  options={optionsfilter} //if category typeoptions, systems options, ipSwitch lista de switchs
                  isMulti={false}
                  isSearchable={false}
                  value={this.state.filterValue}
                  isClearable={true}
                  placeholder='Selecione'
              />
          </div>
        );
    }
  }*/

  render() {
    const { title } = this.props;

    return (
      <EuiPage>
        <EuiPageBody>
          <EuiPageContent>
            <EuiTitle size="l">
              <h1> Dispositivos</h1>
            </EuiTitle>
            <EuiPageContentBody>
          <br/>
           <UncontrolledAlert color="warning" fade={false} style={{ color: '#856404', backgroundColor: '#fff3cd', borderColor: '#ffeeba'}}>
              Você pode adicionar aos dispositivos listados uma categoria e os sistemas que este dispositivo pertence.
          </UncontrolledAlert>
           <br/>
              { /*this.renderFilter()*/ }


              <div className='table-responsive'>

              { this.renderTable() }

              </div>
            </EuiPageContentBody>
          </EuiPageContent>
        </EuiPageBody>
      </EuiPage>

    );
  }
}

const filtro = [
  { value: 'ip', label: 'IP' },
  { value: 'mac', label: 'MAC' },
  { value: 'port', label: 'Porta' },
  { value: 'ipSwitch', label: 'IP Switch' },
  { value: 'category', label: 'Categoria' },
  { value: 'systems', label: 'Sistemas' }
];

const options = [
  { value: 'cftv', label: 'Câmeras (CFTV)' },
  { value: 'sca', label: 'Catracas (SCA)' },
  { value: 'cabeamento', label: 'Cabeamento' },
  { value: 'nobreak', label: 'Nobreak' },
  { value: 'enfermaria', label: 'Enfermaria' },
  { value: 'redes', label: 'Redes' },
  { value: 'impressora', label: 'Impressora' }
];


const typeoptions = [
  { value: 'cftv', label: 'Câmera' },
  { value: 'sca', label: 'Catraca'},
  { value: 'desktop', label: 'Desktop' },
  { value: 'nobreak', label: 'Nobreak'},
  { value: 'servidor', label: 'Servidor'},
  { value: 'impressora', label: 'Impressora'},
  { value: 'switch', label: 'Switch'},
  { value: 'smartphone', label: 'Smartphone'},
  { value: 'outro', label: 'Outro'}
];


const columns = ipSwitchOptions => {
return [
  {
    Header: 'Nome',
    accessor: 'name',
    width: 110,
    resizable: true,
     filterable: true
  },
  {
    Header: 'OS',
    accessor: 'os',
    width: 100,
    resizable: true,
    filterable: true
  },
  {
    Header: 'Fabricante',
    accessor: 'vendor',
    width: 100,
    resizable: true,
    filterable: true
  },
  {
    Header: 'MAC',
    accessor: 'mac',
    width: 130,
    resizable: true,
    filterable: true
  },
  {
    Header: 'IP',
    accessor: 'ip',
    width: 100,
    resizable: true,
    filterable: true

  },
  {
    Header: 'IP Switch',
    accessor: 'ipSwitch',
    width: 100,
    resizable: true,
    filterable: true,
    filterMethod: (filter, row) => {
      switch(filter.value) {
        case 'all':
          return true;
        default:
          if (row._original.ipSwitch) {
            return row._original.ipSwitch == filter.value;
          }
          return false;
      }
    },
    Filter: ({ filter, onChange }) => {
      return(
        <select
          onChange={event => onChange(event.target.value)}
          style={{ width: '100%' }}
          value={filter ? filter.value : 'all'}
          className='form-control'
        >
          <option value='all'>Todos</option>
          {
            ipSwitchOptions.map(option =>{
              return <option key={option} value={option}>{option}</option>
            })
          }
        </select>
      );
    }

  },
  {
    Header: 'Porta',
    accessor: 'port',
    width: 50,
    resizable: true,
    filterable: true

  },
  {
    Header: 'Categoria',
    accessor: 'categorySelect',
    width: 170,
    resizable: true,
    Cell: ({ original, value, index }) => {
      return value(original.category || [], index);
    },
    filterable: true,
    filterMethod: (filter, row) => {
      switch(filter.value) {
        case 'all':
          return true;
        case 'empty':
          return row._original.category === null || row._original.category.length === 0;
        default:
          if (row._original.category.value) {
            return row._original.category.value == filter.value;
          }
          return false;
      }
    },
    Filter: ({ filter, onChange }) => {
      return(
        <select
          onChange={event => onChange(event.target.value)}
          style={{ width: '100%' }}
          value={filter ? filter.value : 'all'}
          className='form-control'
        >
          <option value='all'>Todos</option>
          {
            typeoptions.map(option =>{
              return <option key={option.value} value={option.value}>{option.label}</option>
            })
          }
          <option value='empty'>Vazios</option>
        </select>
      );
    }
  },
  {
    Header: 'Sistema',
    accessor: 'systemsSelect',
    resizable: true,
    Cell: ({ original, value, index }) => {
      return value(original.systems || [], index);
    },
    filterable: true,
    filterMethod: (filter, row) => {
      switch(filter.value) {
        case 'all':
          return true;
        case 'empty':
          return row._original.systems === null || row._original.systems.length === 0;
        default:
          const hasValue = row._original.systems.find(system => {
            return system.value === filter.value;
          });
          return hasValue ? true : false;
      }
    },
    Filter: ({ filter, onChange }) => {
      return(
        <select
          onChange={event => onChange(event.target.value)}
          style={{ width: '100%' }}
          value={filter ? filter.value : 'all'}
          className='form-control'
        >
          <option value='all'>Todos</option>
          {
            options.map(option =>{
              return <option key={option.value} value={option.value}>{option.label}</option>
            })
          }
          <option value='empty'>Vazios</option>
        </select>
      );
    }
  },
]
};
