export default function (server) {

  server.route({
    path: '/api/users/example',
    method: 'GET',
    handler(req, reply) {
      return { time: (new Date()).toISOString() };
    }
  });

  // rota para deletar usuários
  server.route( {
    path: '/api/users/delete/{id}',
    method: 'DELETE',
    handler: async (req, h) => {
        try {
          var id = req.params.id;
          const result = await server.methods.deleteUser(req, h, id);
          return result;
        } catch (err) {
          throw err;
        }
      }
  });


  // deleta um usuário específico
  server.method('deleteUser', (req, h, id) => {
      return new Promise((resolve, reject) => {
        const dataCluster = server.plugins.elasticsearch.getCluster('data');
        dataCluster.callWithRequest(req, 'transport.request', {
            path: '/users/_doc/' + id,
            method: 'DELETE'
          }).then(res => {
            if ((res.result == "deleted" || res.result == "noop") && res._shards.failed == 0){
               resolve({"success": true, "message": "Usuário deletado com sucesso!"});
             }else{
               resolve({"success": false, "message": "Falha ao deletar o usuário!"});
             }
          });
      });
  });


  // retorna uma lista de usuários cadastrados
  server.method('getListUsers', (req, h) => {
      return new Promise((resolve, reject) => {
          const dataCluster = server.plugins.elasticsearch.getCluster('data');
          dataCluster.callWithRequest(req, 'search', {
            index: "users",
            body: {
               "_source": {
                   "exclude": [
                       "password"
                   ]
               }
           }
          }).then(res => {
            resolve(res.hits.hits)
          });
      });
  });

  // rota para pegar a lista de usuários
  server.route({
     path: '/api/users/list',
     method: 'GET',
     handler: async (req, h) => {
        try {
          const result = await server.methods.getListUsers(req, h);
          return result;
        } catch (err) {
          throw err;
        }
     }
   });

}
