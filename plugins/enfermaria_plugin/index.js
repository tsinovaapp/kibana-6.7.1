import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'enfermaria_plugin',
    uiExports: {
      app: {
        title: 'Enfermaria',
        description: 'Chamado enfermaria ',
        main: 'plugins/enfermaria_plugin/app',
        styleSheetPath: require('path').resolve(__dirname, 'public/app.scss'),
        url: "/app/kibana#/dashboard/enfermaria",
        icon: 'plugins/enfermaria_plugin/icon.svg',
      },
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
