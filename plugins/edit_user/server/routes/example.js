export default function (server) {

  server.route({
    path: '/api/editUser/example',
    method: 'GET',
    handler(req, reply) {
      reply({ time: (new Date()).toISOString() });
    }
  });


  // atualiza o cadastro de um usuário
  server.method('updateUser', (req, h, data) => {
      return new Promise((resolve, reject) => {
        const dataCluster = server.plugins.elasticsearch.getCluster('data');
        dataCluster.callWithRequest(req, 'transport.request', {
          path: '/users/_doc/'+req.payload.id+'/_update',
          method: 'POST',
          body: data
        }).then(res2 => {
         if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
            resolve({"success": true, "message": "Cadastro atualizado com sucesso!"});
          }else{
            resolve({"success": false, "message": "Falha ao atualizar o cadastro!"});
          }
        });
      });
  });

  // rota para atualizar o cadastro de um usuário
  server.route({
    path: '/api/users/update',
    method: 'PUT',
    handler: async (req, h) => {

      try {

        if (req.payload.name == undefined || req.payload.login == undefined
        || req.payload.email == undefined){
          return {"success": false, "message": "Informar o nome, login e email!"};
        }
        if (req.payload.id == undefined || req.payload.id == ""){
          return {"success": false, "message": "Não foi informado o id do usuário!"};
        }
        if (req.payload.password != undefined && req.payload.password == ""){
          return {"success": false, "message": "Não foi informada a senha!"};
        }

        var data;

        // foi informada uma nova senha
        if (req.payload.password != undefined){
          data = {
            "doc": {
              "email": req.payload.email,
              "name": req.payload.name,
              "login": req.payload.login,
              "password": req.payload.password
            }
          };
        }else{
          data = {
            "doc": {
              "email": req.payload.email,
              "name": req.payload.name,
              "login": req.payload.login
            }
          };
        }
        const result = await server.methods.updateUser(req, h, data);
        return result;
      } catch (err) {
        throw err;
      }

      /*const dataCluster = server.plugins.elasticsearch.getCluster('data');

      dataCluster.callWithRequest(req, 'transport.request', {
        path: '/users/_doc/'+req.payload.id+'/_update',
        method: 'POST',
        body: data
      }).then(res2 => {
       if ((res2.result == "updated" || res2.result == "noop") && res2._shards.failed == 0){
          reply({"success": true, "message": "Cadastro atualizado com sucesso!"});
        }else{
          reply({"success": false, "message": "Falha ao atualizar o cadastro!"});
        }

      });*/

    }

  });

}
