import exampleRoute from './server/routes/example';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'cadastrar',
    uiExports: {
      app: {
        title: 'Cadastrar',
        description: 'Cadastro de Usuário',
        main: 'plugins/cadastrar/app',
        listed: false
      },
      hacks: [
        'plugins/cadastrar/hack'
      ]
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      exampleRoute(server);
    }
  });
}
