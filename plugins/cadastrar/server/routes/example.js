export default function (server) {

  server.route({
    path: '/api/cadastrar/example',
    method: 'GET',
    handler(req, reply) {
      reply({ time: (new Date()).toISOString() });
    }
  });


  // cadastra um usuário
  server.method('addUser', (req, h) => {
      return new Promise((resolve, reject) => {
        const dataCluster = server.plugins.elasticsearch.getCluster('data');
        dataCluster.callWithRequest(req, 'search', {
          index: "users",
          body: {
              "query": {
                  "match": {
                      "email.keyword": {
                          "query": req.payload.email
                      }
                  }
              }
           }
         }).then(res => {
             if (res.hits.total > 0) {
               resolve({"success": false, "message": "Já existe um usuário cadastrado com esse email!"});
             }else{
               dataCluster.callWithRequest(req, 'transport.request', {
                 path: '/users/_doc/',
                 method: 'POST',
                 body: req.payload
               }).then(res2 => {
               if ((res2.result == "created" || res2.result == "noop") && res2._shards.failed == 0) {
                 resolve({ "success": true, "message": "Usuário cadastrado com sucesso!" });
               }else{
                 resolve({ "success": false, "message": "Falha ao cadastrar o usuário!" });
               }
             });
           }
         });
      });
  });

  // rota para add usuário
  server.route({
    path: '/api/users/add',
    method: 'POST',
    handler: async (req, h) => {

      if (req.payload.name == undefined || req.payload.login == undefined
      || req.payload.password == undefined || req.payload.email == undefined){
        return {"success": false, "message": "Informar o nome, login, senha e email."};
      }

      try {
        const result = await server.methods.addUser(req, h);
        return result;
      } catch (err) {
        throw err;
      }

      /*const dataCluster = server.plugins.elasticsearch.getCluster('data');

      dataCluster.callWithRequest(req, 'search', {
        index: "users",
        body: {
            "query": {
                "match": {
                    "email.keyword": {
                        "query": req.payload.email
                    }
                }
            }
         }

       }).then(res => {
           // email duplicado
           if (res.hits.total > 0) {
             reply({"success": false, "message": "Já existe um usuário cadastrado com esse email!"});

           }else{

             dataCluster.callWithRequest(req, 'transport.request', {
               path: '/users/_doc/',
               method: 'POST',
               body: req.payload
             }).then(res2 => {
             if ((res2.result == "created" || res2.result == "noop") && res2._shards.failed == 0) {
               reply({ "success": true, "message": "Usuário cadastrado com sucesso!" });
             }else{
               reply({ "success": false, "message": "Falha ao cadastrar o usuário!" });
             }
           });
         }
       });*/

     }
   });

}
